package com.jpmc.theater;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.NonNull;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class Theater {

    private final List<Showing> schedule;

    public Theater(@NonNull Supplier<List<Showing>> provider) {
        schedule = provider.get();
    }

    /**
     * Make a reservation
     *
     * @param customer       who is making the reservation
     * @param sequence       the sequence of the movie
     * @param howManyTickets tickets
     * @return the reservation instance
     * @throws IllegalArgumentException in case that the sequence is not valid
     */
    public Reservation reserve(Customer customer, int sequence, int howManyTickets) {
        int indx = sequence - 1;
        if (indx < 0 || indx >= schedule.size()) {
            throw new IllegalArgumentException("not able to find any showing for given sequence " + sequence);
        }
        if (howManyTickets <= 0) {
            throw new IllegalArgumentException("Not valid number of tickets, got " + howManyTickets);
        }
        Showing showing = schedule.get(indx);
        return new Reservation(customer, showing, howManyTickets);
    }

    public void printSchedule() {
        System.out.println(LocalDateTime.now());
        System.out.println("===================================================");
        schedule.forEach(s ->
                System.out.println(s.getSequenceOfTheDay() + ": " + s.getShowStartTime() + " " + s.getMovie().getTitle() + " " + humanReadableFormat(s.getMovie().getRunningTime()) + " $" + s.getMovieFee())
        );
        System.out.println("===================================================");
    }

    public void printScheduleAsJson() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.findAndRegisterModules();
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            System.out.println("=========================JSON==========================");
            System.out.println(objectMapper.writeValueAsString(schedule));
            System.out.println("=======================================================");
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public String humanReadableFormat(Duration duration) {
        long hour = duration.toHours();
        long remainingMin = duration.toMinutes() - TimeUnit.HOURS.toMinutes(duration.toHours());

        return String.format("(%s hour%s %s minute%s)", hour, handlePlural(hour), remainingMin, handlePlural(remainingMin));
    }

    // (s) postfix should be added to handle plural correctly
    private String handlePlural(long value) {
        return value == 1 ? "" : "s";
    }

    public static void main(String[] args) {
        // add the schedule outside the theater constructor
        Supplier<List<Showing>> scheduleSupplier = () -> {
            Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 12.5, 1);
            Movie turningRed = new Movie("Turning Red", Duration.ofMinutes(85), 11, 0);
            Movie theBatMan = new Movie("The Batman", Duration.ofMinutes(95), 9, 0);
            return List.of(
                    new Showing(turningRed, 1, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0))),
                    new Showing(spiderMan, 2, LocalDateTime.of(LocalDate.now(), LocalTime.of(11, 0))),
                    new Showing(theBatMan, 3, LocalDateTime.of(LocalDate.now(), LocalTime.of(12, 50))),
                    new Showing(turningRed, 4, LocalDateTime.of(LocalDate.now(), LocalTime.of(14, 30))),
                    new Showing(spiderMan, 5, LocalDateTime.of(LocalDate.now(), LocalTime.of(16, 10))),
                    new Showing(theBatMan, 6, LocalDateTime.of(LocalDate.now(), LocalTime.of(17, 50))),
                    new Showing(turningRed, 7, LocalDateTime.of(LocalDate.now(), LocalTime.of(19, 30))),
                    new Showing(spiderMan, 8, LocalDateTime.of(LocalDate.now(), LocalTime.of(21, 10))),
                    new Showing(theBatMan, 9, LocalDateTime.of(LocalDate.now(), LocalTime.of(23, 0)))
            );
        };

        Theater theater = new Theater(scheduleSupplier);
        theater.printSchedule();
        theater.printScheduleAsJson();
    }
}
