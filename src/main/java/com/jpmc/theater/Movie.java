package com.jpmc.theater;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jpmc.theater.discount.DiscountEngine;
import com.jpmc.theater.jackson.DurationSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Duration;
import java.util.NoSuchElementException;

@Data
@AllArgsConstructor
public class Movie {

    private String title;

    @JsonSerialize(using = DurationSerializer.class)
    private Duration runningTime;
    private double ticketPrice;

    private int specialCode;

    /**
     * @param showing the showing
     * @return ticket price including the discount
     */
    public double calculateTicketPrice(Showing showing) {
        return ticketPrice - getDiscount(showing);
    }

    private double getDiscount(Showing showing) {
        return DiscountEngine.getRules()
                .stream()
                .mapToDouble(r -> r.apply(showing))
                .max()
                .orElseThrow(NoSuchElementException::new);
    }
}