package com.jpmc.theater.discount;

import com.jpmc.theater.Movie;
import com.jpmc.theater.Showing;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.function.Function;

/**
 * The current class is holder of all discount rules
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DiscountEngine {

    private static final int MOVIE_CODE_SPECIAL = 1;

    private static final Double NO_DISCOUNT = 0.0;

    public static List<Function<Showing, Double>> getRules() {
        return rules;
    }

    /**
     * 20% discount for special movie
     */
    private static final Function<Showing, Double> SPECIAL_MOVIE = (showing) -> {
        Movie movie = showing.getMovie();
        if (MOVIE_CODE_SPECIAL == movie.getSpecialCode()) {
            return movie.getTicketPrice() * 0.2;  // 20% discount for special movie
        }
        return NO_DISCOUNT;
    };

    /**
     * 3$ discount for the first showing of the day
     */
    private static final Function<Showing, Double> FIRST_DAY = (showing) -> {
        if (showing.getSequenceOfTheDay() == 1) {
            return 3.0; // 3$
        }
        return NO_DISCOUNT;
    };

    /**
     * 2$ discount for the second showing of the day
     */
    private static final Function<Showing, Double> SECOND_DAY = (showing) -> {
        if (showing.getSequenceOfTheDay() == 2) {
            return 2.0;
        }
        return NO_DISCOUNT;
    };


    /**
     * 25% discount for movies between 11am-4pm
     */
    private static final Function<Showing, Double> MIDDAY = (showing) -> {
        LocalTime start = LocalTime.of(10, 59);
        LocalTime end = LocalTime.of(16, 1);
        LocalTime showTime = showing.getShowStartTime()
                .toLocalTime();

        if (showTime.isAfter(start) && showTime.isBefore(end)) {
            return showing.getMovie().getTicketPrice() * 0.25;
        }
        return NO_DISCOUNT;
    };

    /**
     * 1$ discount of movies shown on 7th
     */
    private static final Function<Showing, Double> SEVENTH_DAY = (showing) -> {
        LocalDate startDate = showing.getShowStartTime()
                .toLocalDate();
        if (startDate.getDayOfMonth() == 7) {
            return 1.0;
        }
        return NO_DISCOUNT;
    };

    private static final List<Function<Showing, Double>> rules = List.of(SPECIAL_MOVIE, FIRST_DAY, SECOND_DAY, MIDDAY, SEVENTH_DAY);

}
