package com.jpmc.theater;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class Showing {
    private Movie movie;

    private int sequenceOfTheDay;

    private LocalDateTime showStartTime;

    /**
     * @return the price of the ticket <b>without</b> any discount
     */
    public double getMovieFee() {
        return movie.getTicketPrice();
    }

    /**
     * @param audienceCount the audience count
     * @return the total price for everyone of <tt>audienceCount</tt>, the discount is applied for each ticket
     */
    public double calculateFee(int audienceCount) {
        return movie.calculateTicketPrice(this) * audienceCount;
    }
}
