package com.jpmc.theater;


import lombok.*;

@AllArgsConstructor
@Data
public class Reservation {

    @NonNull
    private Customer customer;
    @NonNull
    private Showing showing;
    @NonNull
    private int audienceCount;

    /**
     * @return the total fee including the discounts (if any)
     */
    public double totalFee() {
        return showing.calculateFee(audienceCount);
    }
}