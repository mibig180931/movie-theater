package com.jpmc.theater;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.jpmc.theater.TestScheduleProvider.TEST_SCHEDULE_SUPPLIER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TheaterTests {
    @Test
    void totalFeeForCustomer() {
        Theater theater = new Theater(TEST_SCHEDULE_SUPPLIER);
        Customer john = new Customer("John Doe", "id-12345");
        Reservation reservation = theater.reserve(john, 2, 4);
        assertEquals(37.5, reservation.totalFee());
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 0, 999})
    void reservationWithInvalidSequenceShouldThrowException(int invalidSequence) {
        Theater theater = new Theater(TEST_SCHEDULE_SUPPLIER);
        Customer john = new Customer("John Doe", "id-12345");
        assertThrows(IllegalArgumentException.class, () -> theater.reserve(john, invalidSequence, 4));
    }

    @Test
    void reservationWithZeroTicketsShouldThrowException() {
        Theater theater = new Theater(TEST_SCHEDULE_SUPPLIER);
        Customer john = new Customer("John Doe", "id-12345");
        assertThrows(IllegalArgumentException.class, () -> theater.reserve(john, 2, 0));
    }

    @Test
    void printingMovieShouldNotThrowException() {
        Theater theater = new Theater(TEST_SCHEDULE_SUPPLIER);
        theater.printSchedule();
    }

    @Test
    void printingMovieInJsonShouldNotThrowException() {
        Theater theater = new Theater(TEST_SCHEDULE_SUPPLIER);
        theater.printScheduleAsJson();
    }
}
