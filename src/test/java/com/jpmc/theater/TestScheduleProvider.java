package com.jpmc.theater;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.function.Supplier;

/**
 * Provider of dummy values
 */
public class TestScheduleProvider  {

    public static final LocalDate DEFAULT_TEST_DATE = LocalDate.of(2022, 1, 1);

    public static final LocalDateTime DEFAULT_TEST_TIME = LocalDateTime.of(2022, 1, 1, 22, 0);

    public static Supplier<List<Showing>> TEST_SCHEDULE_SUPPLIER = () -> {
        Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 12.5, 1);
        Movie turningRed = new Movie("Turning Red", Duration.ofMinutes(85), 11, 0);
        Movie theBatMan = new Movie("The Batman", Duration.ofMinutes(95), 9, 0);
        return List.of(
                new Showing(turningRed, 1, LocalDateTime.of(DEFAULT_TEST_DATE, LocalTime.of(9, 0))),
                new Showing(spiderMan, 2, LocalDateTime.of(DEFAULT_TEST_DATE, LocalTime.of(11, 0))),
                new Showing(theBatMan, 3, LocalDateTime.of(DEFAULT_TEST_DATE, LocalTime.of(12, 50))),
                new Showing(turningRed, 4, LocalDateTime.of(DEFAULT_TEST_DATE, LocalTime.of(14, 30))),
                new Showing(spiderMan, 5, LocalDateTime.of(DEFAULT_TEST_DATE, LocalTime.of(16, 10))),
                new Showing(theBatMan, 6, LocalDateTime.of(DEFAULT_TEST_DATE, LocalTime.of(17, 50))),
                new Showing(turningRed, 7, LocalDateTime.of(DEFAULT_TEST_DATE, LocalTime.of(19, 30))),
                new Showing(spiderMan, 8, LocalDateTime.of(DEFAULT_TEST_DATE, LocalTime.of(21, 10))),
                new Showing(theBatMan, 9, LocalDateTime.of(DEFAULT_TEST_DATE, LocalTime.of(23, 0)))
        );
    };

}
