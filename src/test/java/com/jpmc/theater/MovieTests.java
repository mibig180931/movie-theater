package com.jpmc.theater;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MovieTests {
    @ParameterizedTest
    @MethodSource("provideValidShowingsWithDiscount")
    void discountShouldBeIncludedInTheTicketPrice(Showing showing, double expectedPrice) {
        Movie movie = showing.getMovie();
        assertEquals(expectedPrice, movie.calculateTicketPrice(showing));
    }

    @ParameterizedTest
    @MethodSource("provideShowingWithMultipleDiscounts")
    void showingWithMultipleDiscountsShouldApplyTheBiggestDiscount(Showing showing, double expectedPrice) {
        Movie movie = showing.getMovie();
        assertEquals(expectedPrice, movie.calculateTicketPrice(showing));
    }

    public static Stream<Arguments> provideShowingWithMultipleDiscounts() {
        return Stream.of(
                // special movie for the first showing of the day - the special discount should be applied
                Arguments.of(
                        new Showing(createMovie(15.5, 1), 1, showingDateTime()),
                        12.4
                ),
                // special movie for the first showing of the day - the first showing of the day discount should be applied
                Arguments.of(
                        new Showing(createMovie(8.9, 1), 1, showingDateTime()),
                        5.9
                ),
                // special movie for the second showing - the special discount should be applied
                Arguments.of(
                        new Showing(createMovie(8.9, 1), 2, showingDateTime()),
                        6.9
                ),
                // special movie for the second showing - the showing discount should be applied
                Arguments.of(
                        new Showing(createMovie(15.9, 1), 2, showingDateTime()),
                        12.72
                ),
                // movie shown at 7th in 1pm with ticket price 100 - the 11am-4pm discount should be applied
                Arguments.of(
                        new Showing(createMovie(100, 1), 2, showingDateTime(7, 13, 0)),
                        75
                )
        );
    }

    public static Stream<Arguments> provideValidShowingsWithDiscount() {
        return Stream.of(
                // discount with special code
                Arguments.of(
                        new Showing(createMovie(12.5, 1), 5, showingDateTime()),
                        10
                ),
                // discount for the first showing of the day
                Arguments.of(
                        new Showing(createMovie(12.5, 2), 1, showingDateTime()),
                        9.5
                ),
                // discount for the second showing of the day
                Arguments.of(
                        new Showing(createMovie(12.5, 2), 2, showingDateTime()),
                        10.5
                ),
                // no discount
                Arguments.of(
                        new Showing(createMovie(12.5, 999), 999, showingDateTime()),
                        12.5
                ),
                // movie starting between 11-4 should get 25% discount
                Arguments.of(
                        new Showing(createMovie(12.5, 999), 999, showingDateTime(1, 11, 0)),
                        9.375
                ),
                // movie starting between 11-4 should get 25% discount
                Arguments.of(
                        new Showing(createMovie(12.5, 999), 999, showingDateTime(1,14, 30)),
                        9.375
                ),
                // movie starting between 11-4 should get 25% discount
                Arguments.of(
                        new Showing(createMovie(12.5, 999), 999, showingDateTime(1, 16, 0)),
                        9.375
                ),
                // movie showing at 7th get 1$ discount
                Arguments.of(
                        new Showing(createMovie(12.5, 999), 999, showingDateTime(7, 20, 0)),
                        11.5
                )
        );
    }

    private static LocalDateTime showingDateTime() {
        return showingDateTime(1, 10, 0);
    }

    private static LocalDateTime showingDateTime(int dayOfMonth, int hour, int minute) {
        return LocalDateTime.of(2022, 1, dayOfMonth, hour, minute);
    }

    private static Movie createMovie(double ticketPrice, int specialCode) {
        return new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), ticketPrice, specialCode);
    }
}
