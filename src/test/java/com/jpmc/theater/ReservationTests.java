package com.jpmc.theater;

import org.junit.jupiter.api.Test;

import java.time.Duration;

import static com.jpmc.theater.TestScheduleProvider.DEFAULT_TEST_TIME;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReservationTests {

    @Test
    void totalFeeForReservationShouldReturnCorrectValue() {
        var customer = new Customer("John Doe", "unused-id");
        var showing = new Showing(
                new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 12.5, 1),
                1,
                DEFAULT_TEST_TIME
        );
        assertEquals(28.5, new Reservation(customer, showing, 3).totalFee());
    }

}
